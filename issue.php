<?php

/**
 * @Issue(
 *   "An issue about a problem that does not exist"
 *   type="feature"
 *   priority="low"
 *   labels="testing"
 * )
 */

print 'hello world';
print 'something else before exiting';
exit();
